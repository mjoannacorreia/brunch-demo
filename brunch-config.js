module.exports = {
    files: {
        javascripts: { joinTo: 'js/main.js' },
        stylesheets: { joinTo: 'styles/main.css' }
    },
    paths: {
        public: 'dist',
        watched: ['src', 'assets', 'js', 'styles']
    },
    overrides: {
        production: {
            optimize: true,
            sourceMaps: false,
            plugins: { autoReload: { enabled: true } }
        }
    },
    plugins: {
        babel: {
            presets: ['env']
        },
        postcss: {
            processors: [
                // require('postcss-cssnext'),
                require('postcss-simple-vars'),
                // require('postcss-nested'),
                // require('postcss-clean')()
            ]
        },
        uglify: {
            mangle: false,
            compress: {
                global_defs: {
                    DEBUG: false
                }
            }
        },
        htmlPages: {
            htmlMin: {
                caseSensitive: true,
                removeComments: false,
            },
            destination(path) {
                return path.replace(/^src[\/\\](.*)\.html$/, "$1.html");
            },
            disabled: false,
            compileAssets: true,
        }
    }
};